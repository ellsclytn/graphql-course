import React from 'react'
import ReactDOM from 'react-dom'
import { HashRouter as Router, Route, Switch } from 'react-router-dom'
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from 'react-apollo'
import './style/style.css'

import SongList from './components/SongList'
import SongCreate from './components/SongCreate'
import SongDetail from './components/SongDetail'

const client = new ApolloClient({})

const Root = () => (
  <ApolloProvider client={client}>
    <Router>
      <div className='container'>
        <Route exact path='/' component={SongList} />
        <Switch>
          <Route path='/songs/new' component={SongCreate} />
          <Route path='/songs/:id' component={SongDetail} />
        </Switch>
      </div>
    </Router>
  </ApolloProvider>
)

ReactDOM.render(<Root />, document.querySelector('#root'))
