import React from 'react'
import { graphql } from 'react-apollo'
import { Link } from 'react-router-dom'

import fetchSong from '../../queries/fetchSong'
import LyricCreate from '../LyricCreate'
import LyricList from '../LyricList'

const SongDetail = ({ data: { song }, match }) => {
  if (!song) {
    return <p>loading...</p>
  }

  return (
    <div>
      <Link to='/'>Back</Link>
      <h3>{song.title}</h3>
      <LyricList lyrics={song.lyrics} />
      <LyricCreate songId={match.params.id} />
    </div>
  )
}

export default graphql(fetchSong, {
  options: props => ({
    variables: { id: props.match.params.id }
  })
})(SongDetail)
