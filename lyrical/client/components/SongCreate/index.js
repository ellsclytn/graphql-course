import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import { Link } from 'react-router-dom'

import query from '../../queries/fetchSongs'

class SongCreate extends Component {
  constructor (props) {
    super(props)
    this.state = { title: '' }
  }

  onSubmit = e => {
    e.preventDefault()
    this.props
      .mutate({
        variables: {
          title: this.state.title
        },
        refetchQueries: [{ query }]
      })
      .then(() => {
        this.props.history.push('/')
      })
  }

  render () {
    return (
      <div>
        <Link to='/'>Back</Link>
        <h3>Create a New Song</h3>
        <form onSubmit={this.onSubmit}>
          <label>
            Song Title
            <input
              type='text'
              onChange={({ target }) => this.setState({ title: target.value })}
              value={this.state.title}
            />
          </label>
        </form>
      </div>
    )
  }
}

// Throw in an `id` to return here and you could kill `refetchQueries` above ;)
const mutation = gql`
  mutation AddSong($title: String) {
    addSong(title: $title) {
      title
    }
  }
`

export default graphql(mutation)(SongCreate)
