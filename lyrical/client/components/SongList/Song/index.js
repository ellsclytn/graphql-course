import React from 'react'
import { Link } from 'react-router-dom'

const Song = ({ onDelete, title, id }) => (
  <li className='collection-item'>
    <Link to={`/songs/${id}`}>{title}</Link>
    <i className='material-icons' onClick={() => onDelete(id)}>
      delete
    </i>
  </li>
)

export default Song
