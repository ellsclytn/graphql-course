import React, { PureComponent } from 'react'
import { graphql } from 'react-apollo'
import { Link } from 'react-router-dom'
import gql from 'graphql-tag'

import Song from './Song'
import query from '../../queries/fetchSongs'

class SongList extends PureComponent {
  onDelete = id => {
    this.props
      .mutate({ variables: { id } })
      .then(() => this.props.data.refetch())
  }

  render () {
    const { songs = [], loading } = this.props.data

    if (loading) {
      return <p>loading...</p>
    }

    return (
      <section>
        <h1>Song list</h1>
        <ul className='collection'>
          {songs.map(({ id, title }) => (
            <Song key={id} title={title} onDelete={this.onDelete} id={id} />
          ))}
        </ul>
        <Link to='/songs/new' className='btn-floating btn-large red right'>
          <i className='material-icons'>add</i>
        </Link>
      </section>
    )
  }
}

const mutation = gql`
  mutation DeleteSong($id: ID) {
    deleteSong(id: $id) {
      id
    }
  }
`

export default graphql(mutation)(graphql(query)(SongList))
