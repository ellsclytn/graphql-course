import React, { Component } from 'react'
import gql from 'graphql-tag'
import { graphql } from 'react-apollo'

class LyricCreate extends Component {
  constructor (props) {
    super(props)

    this.state = { content: '' }
  }

  onSubmit = e => {
    e.preventDefault()

    this.props
      .mutate({
        variables: {
          content: this.state.content,
          songId: this.props.songId
        }
      })
      .then(() => this.setState({ content: '' }))
  }

  render () {
    return (
      <form onSubmit={this.onSubmit}>
        <label>
          Add a Lyric
          <input
            type='text'
            value={this.state.content}
            onChange={({ target }) => this.setState({ content: target.value })}
          />
        </label>
      </form>
    )
  }
}

const mutation = gql`
  mutation AddLyricToSong($content: String, $songId: ID) {
    addLyricToSong(content: $content, songId: $songId) {
      id
      lyrics {
        content
        id
        likes
      }
    }
  }
`

export default graphql(mutation)(LyricCreate)
