const axios = require('axios')
const graphql = require('graphql')
const {
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString
} = graphql

const SERVER_URL = 'http://localhost:3000'

const CompanyType = new GraphQLObjectType({
  name: 'Company',
  fields: () => ({
    id: { type: GraphQLString },
    name: { type: GraphQLString },
    description: { type: GraphQLString },
    users: {
      type: new GraphQLList(UserType),
      resolve: (parentValue, args) =>
        axios
          .get(`${SERVER_URL}/companies/${parentValue.id}/users`)
          .then(({ data }) => data)
    }
  })
})

const UserType = new GraphQLObjectType({
  name: 'User',
  fields: () => ({
    id: { type: GraphQLString },
    firstName: { type: GraphQLString },
    age: { type: GraphQLInt },
    company: {
      type: CompanyType,
      resolve: (parentValue, args) =>
        axios
          .get(`${SERVER_URL}/companies/${parentValue.companyId}`)
          .then(({ data }) => data)
    }
  })
})

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    user: {
      type: UserType,
      args: { id: { type: GraphQLString } },
      resolve: (parentValue, args) =>
        axios.get(`${SERVER_URL}/users/${args.id}`).then(({ data }) => data)
    },
    company: {
      type: CompanyType,
      args: { id: { type: GraphQLString } },
      resolve: (parentValue, args) =>
        axios.get(`${SERVER_URL}/companies/${args.id}`).then(({ data }) => data)
    }
  }
})

const mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addUser: {
      type: UserType,
      args: {
        firstName: { type: new GraphQLNonNull(GraphQLString) },
        age: { type: new GraphQLNonNull(GraphQLInt) },
        companyId: { type: GraphQLString }
      },
      resolve: (parentValue, { firstName, age }) =>
        axios
          .post(`${SERVER_URL}/users`, {
            firstName,
            age
          })
          .then(({ data }) => data)
    },
    deleteUser: {
      type: UserType,
      args: {
        id: { type: GraphQLNonNull(GraphQLString) }
      },
      resolve: (parentValue, { id }) =>
        axios.delete(`${SERVER_URL}/users/${id}`).then(({ data }) => data)
    },
    editUser: {
      type: UserType,
      args: {
        id: { type: GraphQLNonNull(GraphQLString) },
        firstName: { type: GraphQLString },
        age: { type: GraphQLInt },
        companyId: { type: GraphQLString }
      },
      resolve: (parentValue, { id, ...args }) =>
        axios.patch(`${SERVER_URL}/users/${id}`, args).then(({ data }) => data)
    }
  }
})

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation
})
